package com.pdf.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.pdf.pojo.Order;
import com.pdf.pojo.Product;
import com.pdf.pojo.SignatureInfo;
import com.pdf.pojo.User;
import org.junit.Test;


import javax.swing.*;
import java.io.*;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.*;


public class PDFUtilsTest {

    @Test
    public void f3() {
        Map<String, Object> map = new HashMap<>();

        User user = User.builder().username("张三").certType("身份证").certNumber("100014199004021398").
                tel("17509012551").address("上海市虹口区东大名路1000号").account("600000110005555").build();

        Product product = new Product();
        product.setInterest(7.5);
        product.setLoanCompany("");
        product.setLoanAccount("");

        Order order = Order.builder().user(user).product(product).amount(10000).startDate(new Date()).
                expDate(new Date()).usage("测试合同").build();

        map.put("user", user);
        map.put("product", product);
        map.put("order", order);
        map.put("currentDate", new Date());

        PDFUtils utils = new PDFUtils();
        String htmlStr = utils.FTL2HTML("src/main/resources/templates", "contract.ftl", map);

        utils.HTML2PDF(htmlStr, "src/test/resources/output/contract.pdf");


        try {
            //从keystore中获得私钥和证书链
            KeyStore ks = KeyStore.getInstance("PKCS12");
            char[] password = "123456".toCharArray();
            ks.load(new FileInputStream("src/test/resources/cert/myKeystore.p12"), password);
            String alias = (String) ks.aliases().nextElement();
            PrivateKey pk = (PrivateKey) ks.getKey(alias, password);
            Certificate[] chain = ks.getCertificateChain(alias);

            // 证据文件
            HashMap<String, String> attachments = new HashMap<>();
            attachments.put("seal img", "src/test/resources/img/yhwj.png");
            attachments.put("user info", utils.saveInfo("src/test/resources/temp", user));
            attachments.put("order info", utils.saveInfo("src/test/resources/temp", order));
            PDFUtils.addFileAttachment("src/test/resources/output/contract.pdf",
                    "src/test/resources/output/contract_attached.pdf", attachments);

            // 设置签名信息
            SignatureInfo sealInfo = new SignatureInfo();
            sealInfo.setReason("测试签章系统");
            sealInfo.setLocation("上海居家");
            sealInfo.setChain(chain);
            sealInfo.setPk(pk);
            sealInfo.setFieldName("公司签章");
            sealInfo.setSubfilter(MakeSignature.CryptoStandard.CMS);
            sealInfo.setDigestAlgorithm(DigestAlgorithms.SHA1);
            sealInfo.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            sealInfo.setImagePath("src/test/resources/img/test-seal.png");
            sealInfo.setProvider(ks.getProvider().getName());
            sealInfo.setKeyword("银行签章：");

            // 借款人签名
            SignatureInfo userInfo = new SignatureInfo();
            userInfo.setKeyword("借款人（签名）：");
            userInfo.setChain(chain);
            userInfo.setPk(pk);
            userInfo.setImagePath("src/test/resources/temp/sign.png");
            userInfo.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
            userInfo.setFieldName("借款人签名");


            //new一个上边自定义的方法对象，调用签名方法
            PDFUtils.sign("src/test/resources/output/contract_attached.pdf",
                    "src/test/resources/output/contract_signed.pdf",
                    sealInfo, userInfo);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }

    }

    @Test
    public void testWaterMark(){
        String srcPath = "src/test/resources/output/contract.pdf";
        String savePath = "src/test/resources/output/contract_watermark.pdf";

        try {
            FileOutputStream fos = new FileOutputStream(savePath);
            FileInputStream fis = new FileInputStream(srcPath);
            PDFUtils.setWatermark(fos, fis, "贷款合同", 1);
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

}
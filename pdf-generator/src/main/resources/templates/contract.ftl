<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org"
      xmlns:sec="http://www.thymeleaf.org/thymeleaf-extras-springsecurity3">
<head>
    <title>个人贷款合同</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style mce_bogus="1" type="text/css">
        body {font-family: SimSun; background:none;margin-left: auto;margin-right: auto;}
        body,html,p{ font-size:14px; text-indent: 25px;}
        .div-title{text-align: center}
    </style>
</head>

<body>
<p style="font-size: 18px;text-align: center">邮惠万家银行个人贷款合同</p>

<p>尊敬的客户：为了维护您的权益，请在签署本合同前，仔细阅读本合同各条款，尤其是加粗部分。如您对本合同内容及页面提示信息有疑问，请您暂停后续操作；如您通过网络页面或客户端点击同意或确认的按钮或通过其他操作表示接受本合同内容或使用贷款服务，即表示您已阅读本合同的所有条款，并同意接受本合同的全部约定内容。如您有疑问或不明之处，请向中邮邮惠万家银行有限责任公司（下称“贷款人”）咨询，咨询电话：4001188888，或者您也可咨询您的律师和有关专业人士。
</p>
<p>为了保护您的个人信息安全，本合同在展示时部分隐藏了您的身份信息，您知晓并同意本合同项下您的身份信息以您在贷款人处所留存的信息为准，您不得以此为由否认本合同的效力。
</p><br/>


<div class="div-title">第一部分 借款基本信息</div>


<p>借款人姓名：${user.username}</p>
<p>证件类型：${user.certType}</p>
<p>证件号码：${user.certNumber}</p>
<p>联系电话：${user.tel}</p>
<p>联系地址：${user.address}</p>
<p>借款金额：${order.amount}元</p>
<p>借款期限：自贷款发放之日起至${order.expDate?string("yyyy年MM月dd日")}</p>


<p>借款年化利率（单利）：${product.interest}%（即在2021年10月20日一年期的LPR基础上+385.0基点，其中，1个基点=0.01%）
</p>


<p>还款方式：按月付息，到期还本</p>
<p>首次还款日：${order.startDate?string("yyyy年MM月dd日")}</p>
<p>还款日：每月20号</p>
<p>贷款用途：${order.usage}</p>
<p>放款账户：邮惠万家(0347)</p>
<p>默认还款账户：邮惠万家(0347)</p>
<p>贷款人：中邮邮惠万家银行有限责任公司</p>
<p>支付方式：受托支付</p>

<p>委托贷款人将资金先发放到放款账户（资金发放到上述账户日为贷款起息日），并授权贷款人直接将资金划转至受托支付账户，且承诺如在划转过程中需要借款人配合的，借款人自愿履行配合义务。受托支付账户信息：
</p>

<p>受托支付账户户名：   ${product.loanCompany}</p>
<p>受托支付账户：    ${product.loanAccount}</p>
<p>受托支付金额：    ${order.amount}</p><br/>



<div class="div-title">第二部分 借款条款</div>

<p> 条款省略。。。 <br/></p>

<p style="text-indent: 400px">借款人（签名）：</p>
<p style="text-indent: 400px">贷款人：中邮邮惠万家银行有限责任公司</p>
<p style="text-indent: 400px">签署日期：${currentDate?string("yyyy年MM月dd日")}</p>
<p style="text-indent: 400px">银行签章：</p>


</body>
</html>
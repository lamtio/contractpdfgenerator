<#-- freemarker -->

<h3>${msg}</h3>

<h5>布尔类型</h5>
1.?c: ${bool?c} <br>
2.?string: ${bool?string}<br>
3.?string(t|f): ${bool?string("yes","no")}<br>

<h5>日期类型</h5>
1.?date:${date?date} <br>
2.?time:${date?time} <br>
3.?datetime:${date?datetime} <br>
4.?string(e):${date?string("yyyy年MM月dd日HH时mm分ss秒")}

<h5>数值类型</h5>
1.?c:${num?c}
2.?string.currency:${num?string.currency} <br>
3.?string.percent:${num?string.percent} <br>
4.?string["0.##"]:${num?string["0.#"]}

<h5> Sequence</h5>
<#list users as user>
    ${user}
</#list>
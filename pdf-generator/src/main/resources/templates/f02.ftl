<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>合同文件</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style mce_bogus="1" type="text/css">
        body {font-family: SimSun; background:none;margin-left: auto;margin-right: auto;}
        body,html,div,p{ font-size:14px; margin:0; padding:0;}
    </style></head>
<body>
<table style="border-collapse:collapse;margin:10px 0;borderColor:#000000;height:40px;cellPadding:1;width:100%;" align="center" border="1" >
<tr><td><h1 style="font-size:16px;font-weight:700;margin:6px 0;">三、合同金额</h1></td></tr>
<tr><td>合同金额：</td></tr>
<tr><td>人民币  ${capitalMoney}  大写</td></tr>
<tr><td>人民币 ${lowercaseMoney}  小写</td></tr>
</table>
<table style="border-collapse:collapse;margin:10px 0;borderColor:#000000;height:40px;cellPadding:1;width:100%;" align="center" border="1" >
<tr><td style="width:400px;height:50px;">电    话：${phone}</td><td style="width:400px;height:50px;"> 电    话：###phone2###</td></tr>
<tr><td style="width:400px;height:50px;">签订时间：###signingTime###</td><td style="width:400px;height:50px;"> 签订时间：###signingTime###</td></tr>
</table>
<p style="margin: 60px 0;">此处加盖代理机构合同审核章，并签署日期（同时甲方、乙方、代理机构加盖骑缝章）</p>
<h1 style="font-size:16px;font-weight:700;margin:6px 0;text-align: center;">成 交 货 物 清 单</h1>

<table style="border-collapse:collapse;margin:10px 0;borderColor:#000000;height:40px;cellPadding:1;width:100%;" align="center" border="1" >
    <tr>
        <td>序号</td>
        <td>品目</td>
        <td>品牌</td>
        <td>产品型号</td>
        <td>单价</td>
        <td>数量</td>
        <td>金额（元）</td>
    </tr>
    ###ordersList###
</table>
</body>
</html>

package com.pdf.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/4/1
 */

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User {
    public int userId;
    public String username;
    public String certType;
    public String certNumber;
    public String tel;
    public String address;
    public String account;
    public String email;

}

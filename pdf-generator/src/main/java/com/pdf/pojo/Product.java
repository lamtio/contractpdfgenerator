package com.pdf.pojo;

import lombok.Data;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/4/1
 */
@Data
public class Product {
    public int productId;
    public String productName;
    public double interest;
    public String loanCompany;
    public String loanAccount;
}

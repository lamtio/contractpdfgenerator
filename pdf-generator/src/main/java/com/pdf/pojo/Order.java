package com.pdf.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/4/1
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Order {
    public int orderId;
    public User user;
    public Product product;
    public int amount;
    public Date startDate;
    public Date expDate;
    public String usage;

}

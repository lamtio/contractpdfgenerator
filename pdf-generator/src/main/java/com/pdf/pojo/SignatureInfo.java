package com.pdf.pojo;

import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.MakeSignature;
import lombok.Data;
import org.springframework.lang.NonNullApi;

import java.security.PrivateKey;
import java.security.Provider;
import java.security.cert.Certificate;
import java.util.Map;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/4/19
 */

@Data
public class SignatureInfo {
    private String reason;
    private String location;
    private String imagePath;
    private String fieldName;
    private String provider;
    private Certificate[] chain;
    private PrivateKey pk;
    private String digestAlgorithm = DigestAlgorithms.SHA1;
    private PdfSignatureAppearance.RenderingMode renderingMode = PdfSignatureAppearance.RenderingMode.GRAPHIC;
    private MakeSignature.CryptoStandard subfilter = MakeSignature.CryptoStandard.CMS;
    private String keyword;

}

package com.pdf.utils;

import com.itextpdf.awt.geom.Rectangle2D;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.itextpdf.text.pdf.parser.ImageRenderInfo;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.RenderListener;
import com.itextpdf.text.pdf.parser.TextRenderInfo;
import com.itextpdf.text.pdf.security.*;
import com.pdf.pojo.SignatureInfo;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.xhtmlrenderer.pdf.ITextFontResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;
import sun.misc.BASE64Decoder;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.security.GeneralSecurityException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/3/16
 */

public class PDFUtils {

    /**
     * ftl -> html
     *
     * @param srcPath
     * @param map
     * @return html以string格式
     */
    public String FTL2HTML(String srcPath, String filename, Map<String, Object> map) {

        try {
            // 将生成的内容写入html文件中
            Configuration conf = new Configuration();
            conf.setDirectoryForTemplateLoading(new File(srcPath));
            conf.setDefaultEncoding("UTF-8");
            conf.setDateFormat("yyyy-MM-dd HH:mm:ss");
            Template template = conf.getTemplate(filename);
            StringWriter stringWriter = new StringWriter();
            BufferedWriter writer = new BufferedWriter(stringWriter);
            template.process(map, writer);
            return stringWriter.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("转换html成功！");
        return null;
    }

    /**
     * html -> pdf
     *
     * @param htmlStr
     * @param destPath
     */
    public void HTML2PDF(String htmlStr, String destPath) {
        try {// 将生成的内容写入html转换成pdf
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            org.w3c.dom.Document doc = builder.parse(new ByteArrayInputStream(htmlStr.getBytes("UTF-8")));

            ITextRenderer renderer = new ITextRenderer();
            // 解决中文支持问题
            ITextFontResolver fontResolver = renderer.getFontResolver();

            // 设置pdf中文字体为宋体
            fontResolver.addFont("src/test/resources/fonts/simsun.ttc", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);

            renderer.setDocument(doc, null);
            renderer.layout();
            OutputStream out = null;

            out = new FileOutputStream(destPath);
            renderer.createPDF(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 实体类信息保存
     *
     * @param savDir 存储目录
     * @param object 实例
     */
    public String saveInfo(String savDir, Object object) {
        FileOutputStream fos = null;
        String savePath = savDir + "/" + object.getClass().getSimpleName() + ".txt";
        File file = new File(savePath);

        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            fos = new FileOutputStream(file);
            fos.write(object.toString().getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return savePath;
    }


    /**
     * 根据关键字和页码  获取关键字的定位
     *
     * @param parse      读取pdf内容对象
     * @param pageNumber 定位的页码
     * @param keyword    关键字
     * @return List<float [ ]> 定位集合     float[]:坐标和页数 float[0] >> X float[1] >> Y float[2] >> page
     */
    static java.util.List<float[]> getKeyWordPositionByPageNum(PdfReaderContentParser parse, Integer pageNumber, String keyword) {
        java.util.List<float[]> list = new ArrayList<float[]>();
        float[] coords = new float[3];
        try {
            parse.processContent(pageNumber, new RenderListener() {
                @Override
                public void renderText(TextRenderInfo renderInfo) {
                    String text = renderInfo.getText();
                    if (null != text && text.contains(keyword)) {
                        Rectangle2D.Float boundingRectangle = renderInfo.getBaseline().getBoundingRectange();
                        coords[0] = boundingRectangle.x;
                        coords[1] = boundingRectangle.y;
                        coords[2] = pageNumber;
                        list.add(coords);
                    }
                }

                @Override
                public void beginTextBlock() {
                }

                @Override
                public void endTextBlock() {
                }

                @Override
                public void renderImage(ImageRenderInfo renderInfo) {
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 全局搜索
     *
     * @param parse   读取pdf内容对象
     * @param pages   pdf总页数
     * @param keyword 关键字
     * @return List<float [ ]> 定位集合    float[]:坐标和页数 float[0] >> X float[1] >> Y float[2] >> page
     */
    public static java.util.List<float[]> getKeyWordPositionByAllPages(PdfReaderContentParser parse, Integer pages, String keyword) {
        java.util.List<float[]> List = new ArrayList<float[]>();
        java.util.List<float[]> List1 = null;
        for (int pageNumber = 1; pageNumber <= pages; pageNumber++) {
            List1 = getKeyWordPositionByPageNum(parse, pageNumber, keyword);
            if (List1.size() != 0) {
                List.addAll(List1);
            }
        }
        return List;
    }


    /**
     * pdf签名
     *
     * @param src            需要签章的pdf文件路径
     * @param dest           签完章的pdf文件路径
     * @param signatureInfos 签名信息类
     */
    public static void sign(String src, String dest, SignatureInfo... signatureInfos) {
        InputStream inputStream = null;
        FileOutputStream outputStream = null;
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        try {
            inputStream = new FileInputStream(src);
            for (SignatureInfo signatureInfo : signatureInfos) {
                ByteArrayOutputStream tempArrayOutputStream = new ByteArrayOutputStream();
                PdfReader reader = new PdfReader(inputStream);
                // 创建签章工具PdfStamper
                PdfStamper stamper = PdfStamper.createSignature(reader, tempArrayOutputStream, '\0', null, true);
                // 获取数字签章属性对象，设定数字签章的属性
                PdfSignatureAppearance appearance = stamper.getSignatureAppearance();

                if (signatureInfo.getLocation() != null) {
                    appearance.setLocation(signatureInfo.getLocation());
                }
                if (signatureInfo.getReason() != null) {
                    appearance.setReason(signatureInfo.getReason());
                }

                // 获取pdf的最后一页页数
                int lastPage = reader.getNumberOfPages();
                // 获得读取pdf内容对象
                PdfReaderContentParser parse = new PdfReaderContentParser(reader);
                // 获得相关页码关键字具体位置 对象
                List<float[]> pl = PDFUtils.getKeyWordPositionByAllPages(parse, lastPage, signatureInfo.getKeyword());
                // 获取最后一个关键字定位对象
                float[] p = null;
                if (pl.size() != 0) {
                    p = pl.get(pl.size() - 1);
                } else {
                    throw new Exception("pdf没有读取到签章关键字！");
                }
                // 设置签名签名域图章大小及位置
                //读取图章图片
                Image image = Image.getInstance(signatureInfo.getImagePath());
                //签名的位置，是图章相对于pdf页面的位置坐标，原点为pdf页面左下角
                //四个参数的分别是，图章左下角x，图章左下角y，图章右上角x，图章右上角y
                Rectangle position = new Rectangle(p[0] + 50, p[1] - 50, p[0] + 150, p[1] + 50);
                // 设置签名的签名域名称
                appearance.setVisibleSignature(position, (int) p[2], signatureInfo.getFieldName());
                appearance.setSignatureGraphic(image);
                appearance.setCertificationLevel(PdfSignatureAppearance.NOT_CERTIFIED);
                //设置图章的显示方式
                appearance.setRenderingMode(PdfSignatureAppearance.RenderingMode.GRAPHIC);
                // 摘要算法
                ExternalDigest digest = new BouncyCastleDigest();
                // 签名算法
                ExternalSignature signature = new PrivateKeySignature(signatureInfo.getPk(), signatureInfo.getDigestAlgorithm(), signatureInfo.getProvider());
                // 调用itext签名方法完成pdf签章
                MakeSignature.signDetached(appearance, digest, signature, signatureInfo.getChain(), null, null, null, 0, signatureInfo.getSubfilter());
                //定义输入流为生成的输出流内容，以完成多次签章的过程
                inputStream = new ByteArrayInputStream(tempArrayOutputStream.toByteArray());
                result = tempArrayOutputStream;

            }
            outputStream = new FileOutputStream(dest);
            outputStream.write(result.toByteArray());
            outputStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                outputStream.close();
                inputStream.close();
                result.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * 添加附件
     *
     * @param src          源文件
     * @param dest         目标文件
     * @param attachments  附件
     */
    public static void addFileAttachment(String src, String dest, Map<String, String> attachments) {

        // 添加附件证据链，[e.g. face_proof.png、proofHash.xml、textEvidence.txt、userInfo.txt]
        if (attachments != null) {
            PdfReader reader = null;
            try {
                reader = new PdfReader(src);
                PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(dest));
                for (String description : attachments.keySet()) {
                    File f = new File(attachments.get(description));
                    PdfFileSpecification fs = PdfFileSpecification.fileEmbedded(stamper.getWriter(), f.getAbsolutePath(), f.getName(), null);
                    stamper.addFileAttachment(description, fs);
                }
                stamper.close();
            } catch (IOException | DocumentException e) {
                e.printStackTrace();
            }


        }
    }


    /**
     * 中间或者两边水印
     * @param bos   添加完水印的输出
     * @param input 原PDF文件输入
     * @param word  水印内容
     * @param model 水印添加位置1中间，2两边
     */
    public static void setWatermark(OutputStream os, InputStream input, String word, int model)
            throws DocumentException, IOException {
        PdfReader reader = new PdfReader(input);
        PdfStamper stamper = new PdfStamper(reader, os);
        PdfContentByte content;
        // 创建字体,第一个参数是字体路径,itext有一些默认的字体比如说：
        BaseFont base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        // BaseFont base = BaseFont.createFont("src/test/resources/fonts/simkai.ttf", BaseFont.IDENTITY_H,
        //        BaseFont.EMBEDDED);
        PdfGState gs = new PdfGState();
        // 获取PDF页数
        int total = reader.getNumberOfPages();
        // 遍历每一页
        for (int i = 0; i < total; i++) {
            float width = reader.getPageSize(i + 1).getWidth(); // 页宽度
            float height = reader.getPageSize(i + 1).getHeight(); // 页高度
            content = stamper.getOverContent(i + 1);// 内容
            content.beginText();//开始写入文本
            gs.setFillOpacity(0.3f);//水印透明度
            content.setGState(gs);
            content.setColorFill(BaseColor.LIGHT_GRAY);
            content.setTextMatrix(70, 200);//设置字体的输出位置

            if (model == 1) { //平行居中的3条水印
                content.setFontAndSize(base, 50); //字体大小
                //showTextAligned 方法的参数分别是（文字对齐方式，位置内容，输出水印X轴位置，Y轴位置，旋转角度）
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 650, 30);
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 400, 30);
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 150, 30);
            } else { // 左右两边个从上到下4条水印
                float rotation = 30;// 水印旋转度数

                content.setFontAndSize(base, 20);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 4 * 3 - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 2 - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 4 - 50, rotation);

                content.setFontAndSize(base, 22);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 4 * 3 - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 2 - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 4 - 50, rotation);
            }
            content.endText();//结束写入文本
            //要打图片水印的话
            //Image image = Image.getInstance("c:/1.jpg");
            //content.addImage(image);
        }
        stamper.close();
    }
}




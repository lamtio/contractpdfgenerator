package com.pdf.controller;

import com.pdf.pojo.Order;
import com.pdf.pojo.Product;
import com.pdf.pojo.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/3/11
 */
@Controller
@RequestMapping("/test")
public class TestController {
    @Value("${user1.name}")
    String name;

    @GetMapping("/hello")
    @ResponseBody
    String hello(){
        return "Hello FreeMarker <br> test properties: " + name;
    }

    @GetMapping("/f01")
    public ModelAndView testF02(HttpServletRequest request) {
        request.setAttribute("msg", "数据类型测试");
        request.setAttribute("bool", true);
        request.setAttribute("date", new Date());
        request.setAttribute("num", 5.67);
        request.setAttribute("users", new String[]{"Adam", "Bell", "Calvin"});
        return new ModelAndView("f01");
    }

    @RequestMapping("/f02")
    public String testF01(ModelMap map) {
        map.addAttribute("lowercaseMoney", 1000);
        map.addAttribute("capitalMoney", "壹仟元");
        map.addAttribute("phone", 1515095555);

        return "f02";
    }

    @RequestMapping("/f03")
    public String register(){
        return "/register.html";
    }

    @RequestMapping("/f04")
    public String testF04(ModelMap map, HttpServletRequest request){
        User user = new User();
        user.setUsername(request.getParameter("username"));
        user.setCertType(request.getParameter("certType"));
        user.setCertNumber(request.getParameter("certNum"));
        user.setTel(request.getParameter("tel"));
        user.setAddress(request.getParameter("address"));
        user.setAccount(request.getParameter("account"));

        Product product = new Product();
        product.setProductName(request.getParameter("productName"));
        product.setInterest(Double.parseDouble(request.getParameter("interest")));
        product.setLoanCompany(request.getParameter("loanCompany"));
        product.setLoanAccount(request.getParameter("loanAccount"));

        Order order = new Order();
        order.setUser(user);
        order.setProduct(product);
        order.setAmount(Integer.parseInt(request.getParameter("amount")));
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            order.setStartDate(sdf.parse(request.getParameter("startDate")));
            order.setExpDate(sdf.parse(request.getParameter("expDate")));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        order.setUsage("测试合同");

        map.addAttribute("user",user);
        map.addAttribute("product",product);
        map.addAttribute("order",order);
        map.addAttribute("currentDate", new Date());
        return "contract";
    }
}

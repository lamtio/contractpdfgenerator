package com.pdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/3/11
 */
@SpringBootApplication
public class PdfApplication {
    public static void main(String[] args) {
        try {
            SpringApplication.run(PdfApplication.class, args);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

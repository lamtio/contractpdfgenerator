package com.pdf;

import com.itextpdf.text.DocumentException;
import com.pdf.utils.PDFUtils;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/5/12
 */
public class UtilsApplication extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    String srcPath;
    String watermarkStr;
    Image icon = new Image(String.valueOf(Thread.currentThread().getContextClassLoader().getResource("img/yhwj.png")));


    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField textField = new TextField();
        textField.setPromptText("请选择pdf文件");
        Button buttonChooseSrcFile = new Button("选择文件");
        buttonChooseSrcFile.setOnAction(event -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("pdf文件", "*.pdf"));
            File file = fileChooser.showOpenDialog(primaryStage);
            srcPath = file.getPath();
            textField.setText(srcPath);
            System.out.println(srcPath);
        });


        TextField textField2 = new TextField();
        textField2.setPromptText("请输入水印文字");
        Button buttonGenerate = new Button("生成");
        buttonGenerate.setOnAction(event -> {
            watermarkStr = textField2.getText();
            String savePath = srcPath.replace(".pdf", "_watermark.pdf");
            File file = new File(savePath);
            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                FileOutputStream fos = new FileOutputStream(savePath);
                FileInputStream fis = new FileInputStream(srcPath);
                PDFUtils.setWatermark(fos, fis, watermarkStr, 1);
                System.out.println(savePath);
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("水印添加成功！");
                alert.setHeaderText(null);
                alert.setContentText(savePath);
                alert.showAndWait();
            } catch (DocumentException | IOException e) {
                e.printStackTrace();
            }
        });

        VBox vBox = new VBox();
        HBox hBox = new HBox();
        hBox.getChildren().add(new Label("路径"));
        hBox.getChildren().add(textField);
        hBox.getChildren().add(buttonChooseSrcFile);
        vBox.getChildren().add(hBox);

        HBox hBox2 = new HBox();
        hBox2.getChildren().add(new Label("水印"));
        hBox2.getChildren().add(textField2);
        hBox2.getChildren().add(buttonGenerate);
        vBox.getChildren().add(hBox2);

        Scene scene = new Scene(vBox, 300, 80);
        primaryStage.setScene(scene);
        primaryStage.setTitle("邮惠万家银行水印工具 by Tianhao");
        primaryStage.getIcons().add(icon);
        primaryStage.show();//显示stage
    }
}

package com.zth;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @description: ISO7064:1983.MOD11-2, 身份证伪校验
 * @author: Tianhao
 * @time: 2022/6/9
 */
public class ValidateIdCardUtil {
    final static Map<Integer, String> zoneNum = new HashMap<>();

    static {
        zoneNum.put(11, "北京");
        zoneNum.put(12, "天津");
        zoneNum.put(13, "河北");
        zoneNum.put(14, "山西");
        zoneNum.put(15, "内蒙古");
        zoneNum.put(21, "辽宁");
        zoneNum.put(22, "吉林");
        zoneNum.put(23, "黑龙江");
        zoneNum.put(31, "上海");
        zoneNum.put(32, "江苏");
        zoneNum.put(33, "浙江");
        zoneNum.put(34, "安徽");
        zoneNum.put(35, "福建");
        zoneNum.put(36, "江西");
        zoneNum.put(37, "山东");
        zoneNum.put(41, "河南");
        zoneNum.put(42, "湖北");
        zoneNum.put(43, "湖南");
        zoneNum.put(44, "广东");
        zoneNum.put(45, "广西");
        zoneNum.put(46, "海南");
        zoneNum.put(50, "重庆");
        zoneNum.put(51, "四川");
        zoneNum.put(52, "贵州");
        zoneNum.put(53, "云南");
        zoneNum.put(54, "西藏");
        zoneNum.put(61, "陕西");
        zoneNum.put(62, "甘肃");
        zoneNum.put(63, "青海");
        zoneNum.put(64, "宁夏");
        zoneNum.put(65, "新疆");
        zoneNum.put(71, "台湾");
        zoneNum.put(81, "香港");
        zoneNum.put(82, "澳门");
        zoneNum.put(91, "外国");
    }

    final static char[] RARITYBIT = {'1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2', '1'};
    final static int[] WEIGHTS = {7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2};

    /**
     * 校验合法性
     *
     * @param certNum
     * @return
     */
    public static Result validCertNum(String certNum) {
        // 校验长度
        if (certNum == null || certNum.length() != 15 && certNum.length() != 18) {
            return new Result("长度不符合", false);
        }

        char[] chars = certNum.toUpperCase().toCharArray();
        int sum = 0;
        // 校验位数
        for (int i = 0; i < chars.length; i++) {
            if (i == chars.length - 1 && chars[i] == 'X') {
                break;
            }
            if (chars[i] < '0' || chars[i] > '9') {
                return new Result("存在错误字符", false);
            }
            if (i < chars.length - 1) {
                sum += (chars[i] - '0') * WEIGHTS[i];
            }
        }

        // 校验区位码
        if (!zoneNum.containsKey(Integer.valueOf(certNum.substring(0, 2)))) {
            return new Result("区位码错误", false);
        }

        // 校验生日
        String birthday = null;
        SimpleDateFormat sdf = null;
        if (certNum.length() == 15) {
            birthday = certNum.substring(6, 12);
            sdf = new SimpleDateFormat("yyMMdd");
        }
        if (certNum.length() == 18) {
            birthday = certNum.substring(6, 14);
            sdf = new SimpleDateFormat("yyyyMMdd");
        }
        try {
            Date date = sdf.parse(birthday);
            if (!sdf.format(date).equals(birthday)) {
                return new Result("无中生有的日子", false);
            }
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            if (calendar.get(Calendar.YEAR) < 1900) {
                return new Result("建国后不准成精", false);
            }
            if (calendar.get(Calendar.YEAR) > Calendar.getInstance().get(Calendar.YEAR)) {
                return new Result("未来之子", false);
            }
        } catch (ParseException e) {
            return new Result("无法获取日期", false);
        }

        // 校验校验码
        if (certNum.length() == 18) {
            if (chars[certNum.length()-1] != RARITYBIT[sum % 11]){
                return new Result("校验码编错了【 " + Character.toString(RARITYBIT[sum % 11]) +"】" , false);
            }
        }
        return new Result("好像没毛病",true);
    }
}

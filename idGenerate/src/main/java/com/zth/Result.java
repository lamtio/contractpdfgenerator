package com.zth;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/6/9
 */
public class Result<T> {
    private String msg;
    private boolean bool;

    public Result() {
    }

    public Result(String msg, boolean bool) {
        this.msg = msg;
        this.bool = bool;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isBool() {
        return bool;
    }

    public void setBool(boolean bool) {
        this.bool = bool;
    }

    @Override
    public String toString() {
        return "Result{" +
                "msg='" + msg + '\'' +
                ", bool=" + bool +
                '}';
    }

    public static <T> ResultBuilder<T> builder() {
        return new ResultBuilder<T>();
    }

    public static class ResultBuilder<T> {
        private String msg;
        private boolean bool;

        public ResultBuilder() {
        }

        public ResultBuilder<T> msg(String msg) {
            this.msg = msg;
            return this;
        }

        public ResultBuilder<T> bool(boolean bool) {
            this.bool = bool;
            return this;
        }

        public Result<T> build() {
            return new Result(this.msg, this.bool);
        }

        @Override
        public String toString() {
            return "ResultBuilder{" +
                    "msg='" + msg + '\'' +
                    ", bool=" + bool +
                    '}';
        }
    }
}

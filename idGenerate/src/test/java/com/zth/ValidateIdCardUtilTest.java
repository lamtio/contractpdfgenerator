package com.zth;

import org.junit.Test;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/6/9
 */
public class ValidateIdCardUtilTest {
    public void testRecord(String cert){
        System.out.println("Cert: " + cert + "  " + ValidateIdCardUtil.validCertNum(cert));
    }

    @Test
    public void testData(){
        testRecord("111000880220345");
        testRecord("111X00880220345");
        testRecord("111000198802203456");
        testRecord("111000198802203452");
        testRecord("11100020230228345X");
        testRecord("11100020230229345X");
        testRecord("11100018990228345X");
        testRecord("310120199209090014");
        testRecord("440101199212253142");
        testRecord("420020199203032224");
    }
}
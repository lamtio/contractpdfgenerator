package com.zth.utils;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;

import java.io.*;


/**
 * @description:
 * @author: Tianhao
 * @time: 2022/3/16
 */

public class Watermark {
    /**
     * 中间或者两边水印
     *
     * @param bos   添加完水印的输出
     * @param input 原PDF文件输入
     * @param word  水印内容
     * @param model 水印添加位置1中间，2两边
     */
    public static void setWatermark(OutputStream os, InputStream input, String word, int model)
            throws DocumentException, IOException {
        PdfReader reader = new PdfReader(input);
        PdfStamper stamper = new PdfStamper(reader, os);
        PdfContentByte content;
        // 创建字体,第一个参数是字体路径,itext有一些默认的字体比如说：
        BaseFont base = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.EMBEDDED);
        PdfGState gs = new PdfGState();
        // 获取PDF页数
        int total = reader.getNumberOfPages();
        // 遍历每一页
        for (int i = 0; i < total; i++) {
            float width = reader.getPageSize(i + 1).getWidth(); // 页宽度
            float height = reader.getPageSize(i + 1).getHeight(); // 页高度
            content = stamper.getOverContent(i + 1);// 内容
            content.beginText();//开始写入文本
            gs.setFillOpacity(0.3f);//水印透明度
            content.setGState(gs);
            content.setColorFill(BaseColor.LIGHT_GRAY);
            content.setTextMatrix(70, 200);//设置字体的输出位置

            if (model == 1) { //平行居中的3条水印
                content.setFontAndSize(base, 50);
                //showTextAligned 方法的参数分别是（文字对齐方式，位置内容，输出水印X轴位置，Y轴位置，旋转角度）
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 650, 30);
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 400, 30);
                content.showTextAligned(Element.ALIGN_CENTER, word, width / 2, 150, 30);
            } else { // 左右两边个从上到下4条水印
                float rotation = 30;

                content.setFontAndSize(base, 20);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 4 * 3 - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 2 - 50, rotation);
                content.showTextAligned(Element.ALIGN_LEFT, word, 20, height / 4 - 50, rotation);

                content.setFontAndSize(base, 22);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 4 * 3 - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 2 - 50, rotation);
                content.showTextAligned(Element.ALIGN_RIGHT, word, width - 20, height / 4 - 50, rotation);
            }
            content.endText();
        }
        stamper.close();
    }


    /*public static void excelToPdf() throws IOException, DocumentException {
        Document document = new Document(PageSize.A4.rotate());

        BaseFont baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
        Font font = new Font(baseFont);
        font.setSize(13);

        PdfWriter writer = PdfWriter.getInstance(document,new FileOutputStream("C:\\Users\\dd\\Desktop\\PdfTable3.pdf"));

        document.open();
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(new File("C:\\Users\\dd\\Desktop\\3234.xls")));

        HSSFSheet sheet = workbook.getSheetAt(0);
        int column = sheet.getRow(0).getLastCellNum();
        int row = sheet.getPhysicalNumberOfRows();

        PdfPTable table = new PdfPTable(column-sheet.getRow(0).getFirstCellNum());

        String str = null;
        for (int i = sheet.getFirstRowNum(); i < row; i++) {
            for (int j = sheet.getRow(0).getFirstCellNum(); j < column; j++) {
                //得到excel单元格的内容
                HSSFCell cell = sheet.getRow(i).getCell(j);
                if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    str = (int)cell.getNumericCellValue() + "";
                }else{
                    str = cell.getStringCellValue();
                }
                //创建pdf单元格对象，并往pdf单元格对象赋值。
                PdfPCell cells = new PdfPCell(new Paragraph(str, font));
                //pdf单元格对象添加到table对象
                table.addCell(cells);
            }
        }

        document.add(table);
        document.close();
        writer.close();
    }*/

}




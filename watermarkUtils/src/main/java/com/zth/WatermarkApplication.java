package com.zth;

import com.itextpdf.text.DocumentException;
import com.zth.utils.Watermark;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @description:
 * @author: Tianhao
 * @time: 2022/5/12
 */
public class WatermarkApplication extends Application {
    private static final String IMAGE_PATH = "img/yhwj.png";
    private String srcPath;
    private final Image icon = new Image(String.valueOf(Thread.currentThread().getContextClassLoader().getResource(IMAGE_PATH)));

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        TextField textField = new TextField();
        textField.setPromptText("请选择pdf文件");
        Button buttonChooseSrcFile = new Button("选择文件");
        buttonChooseSrcFile.setOnAction(event -> {
            chooseSrcFile(primaryStage, textField);
        });

        TextField textField2 = new TextField();
        textField2.setPromptText("请输入水印文字");
        Button buttonGenerate = new Button("生成");
        buttonGenerate.setOnAction(event -> {
            generateWatermark(textField2);
        });

        Button buttonTransferBase64 = new Button("转换");
        TextArea base64Area = new TextArea();
        base64Area.setPromptText("base64值");
        WebView webView = new WebView();
        buttonTransferBase64.setOnAction(event -> {
            transferToBase64(base64Area, webView);
        });

        VBox vBox = new VBox();

        VBox watermarkBox = new VBox();
        TitledPane watermarkPane = new TitledPane("水印小工具", watermarkBox);
        watermarkPane.setPadding(new Insets(5));
        HBox hBox = new HBox();
        hBox.getChildren().add(new Label("路径"));
        hBox.getChildren().add(textField);
        hBox.getChildren().add(buttonChooseSrcFile);
        watermarkBox.getChildren().add(hBox);
        HBox hBox2 = new HBox();
        hBox2.getChildren().add(new Label("水印"));
        hBox2.getChildren().add(textField2);
        hBox2.getChildren().add(buttonGenerate);
        watermarkBox.getChildren().add(hBox2);

        HBox base64Box = new HBox();
        TitledPane base64Pane = new TitledPane("Base64转换图片", base64Box);
        base64Pane.setPadding(new Insets(5));

        base64Box.getChildren().add(base64Area);
        base64Box.getChildren().add(buttonTransferBase64);
        base64Box.getChildren().add(webView);

        Label label = new Label("场景金融IT出品");

        vBox.getChildren().add(watermarkPane);
        vBox.getChildren().add(base64Pane);
        vBox.getChildren().add(label);

        Scene scene = new Scene(vBox, 300, 210);
        primaryStage.setScene(scene);
        primaryStage.setTitle("邮惠万家小工具");
        primaryStage.getIcons().add(icon);
        primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double height = newValue.doubleValue();
                //设置控件的高度（按比例）
                webView.setPrefHeight(height);
                base64Area.setPrefHeight(height * 70 / 735);
            }
        });
        primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                double width = newValue.doubleValue();
                //设置控件的宽度（按比例）
                webView.setPrefWidth(0.45 * width);
                webView.setPrefWidth(0.45 * width);
            }
        });

        primaryStage.show();

        // Load and display the UI, then perform file selection and processing asynchronously.
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                buttonChooseSrcFile.setOnAction(event -> chooseSrcFile(primaryStage, textField));
                buttonGenerate.setOnAction(event -> generateWatermark(textField2));
                buttonTransferBase64.setOnAction(event -> transferToBase64(base64Area, webView));
                return null;
            }
        };
        new Thread(task).start();
    }

    private void transferToBase64(TextArea base64Area, WebView webView) {
        //            <img src= "data:image/jpeg;base64, base64编码内容">
        String base64Str = base64Area.getText();
        String htmlStr = "<img src= \"data:image/jpeg;base64, " + base64Str + "\">";
        webView.getEngine().loadContent(htmlStr);
    }

    private void generateWatermark(TextField textField2) {
        String watermarkStr = textField2.getText();
        String savePath = srcPath.replace(".pdf", "_watermark.pdf");
        File file = new File(savePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            FileOutputStream fos = new FileOutputStream(savePath);
            FileInputStream fis = new FileInputStream(srcPath);
            Watermark.setWatermark(fos, fis, watermarkStr, 1);
            System.out.println(savePath);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("水印添加成功！");
            alert.setHeaderText(null);
            alert.setContentText(savePath);
            alert.showAndWait();
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    private void chooseSrcFile(Stage primaryStage, TextField textField) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("pdf文件", "*.pdf"));
        File file = fileChooser.showOpenDialog(primaryStage);
        srcPath = file.getPath();
        textField.setText(srcPath);
        System.out.println(srcPath);
    }


}
